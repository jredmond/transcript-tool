from peewee import *
from playhouse.shortcuts import model_to_dict
from fsrs import Card
from datetime import datetime

# Connect to a SQLite database
db = SqliteDatabase("words_analysis.db")


class TimestampTzField(Field):
    """
    A timestamp field that supports a timezone by serializing the value
    with isoformat.
    """

    field_type = "TEXT"  # This is how the field appears in Sqlite

    def db_value(self, value: datetime) -> str:
        if value:
            return value.isoformat()

    def python_value(self, value: str) -> str:
        if value:
            return datetime.fromisoformat(value)


class BaseModel(Model):
    class Meta:
        database = db


class Lemma(BaseModel):
    text = CharField(unique=True)


class Sentence(BaseModel):
    text = TextField()
    timestamp = TimeField(null=True)


class WordForm(BaseModel):
    text = CharField()
    pos = CharField()
    morph = CharField(null=True)
    lemma = ForeignKeyField(Lemma, backref="word_forms")
    sentence = ForeignKeyField(Sentence, backref="word_forms")


class SkippedLemma(BaseModel):
    lemma = CharField(unique=True)


class Flashcard(BaseModel):
    lemma = CharField(unique=True)
    due = TimestampTzField()
    stability = FloatField()
    difficulty = FloatField()
    elapsed_days = IntegerField()
    scheduled_days = IntegerField()
    reps = IntegerField()
    lapses = IntegerField()
    state = IntegerField()
    last_review = TimestampTzField(null=True)

    @classmethod
    def create__new_flashcard(cls, lemma_text):
        flashcard = Flashcard()
        flashcard.lemma = lemma_text
        flashcard.from_card(Card())
        return flashcard

    def from_card(self, card):
        self.due = card.due
        self.stability = card.stability
        self.difficulty = card.difficulty
        self.elapsed_days = card.elapsed_days
        self.scheduled_days = card.scheduled_days
        self.reps = card.reps
        self.lapses = card.lapses
        self.state = card.state
        self.last_review = card.last_review if hasattr(card, "last_review") else None

    def to_card(self):
        return Card(
            self.due,
            self.stability,
            self.difficulty,
            self.elapsed_days,
            self.scheduled_days,
            self.reps,
            self.lapses,
            self.state,
            self.last_review,
        )

    def to_dict(self):
        return model_to_dict(self)


# Create the tables
db.connect()
db.create_tables([Lemma, Sentence, WordForm, Flashcard, SkippedLemma], safe=True)
