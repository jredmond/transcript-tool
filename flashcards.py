from fsrs import *
from models import *
from datetime import datetime, timezone
import random
from PySide6.QtWidgets import (
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QPushButton,
    QComboBox,
    QApplication,
    QSpacerItem,
    QSizePolicy,
)
from PySide6.QtGui import QMouseEvent
from PySide6.QtCore import Qt, QTimer
from video_player import VideoPlayerWidget
from text_to_speech import TTS
import webbrowser
import urllib.parse
import re
from googletrans import Translator


class FlashcardWidget(QWidget):
    fsrs = FSRS()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.current_card = None

        self.layout = QVBoxLayout(self)

        self.add_more_area = QWidget()
        self.add_more_layout = QHBoxLayout(self.add_more_area)
        self.add_more_layout.setAlignment(Qt.AlignTop)
        self.layout.addWidget(self.add_more_area)

        self.add_more_cards_button = QPushButton("Add more cards")
        self.add_more_cards_button.clicked.connect(
            lambda: self.add_flashcards(self.pos_combo.currentText())
        )
        self.add_more_layout.addWidget(self.add_more_cards_button)

        self.pos_label = QLabel("Part of Speech:")
        self.add_more_layout.addWidget(self.pos_label)

        self.pos_combo = QComboBox()
        self.pos_combo.addItems(["ALL", "NOUN", "VERB"])
        self.add_more_layout.addWidget(self.pos_combo)

        self.add_more_cards_button = QPushButton("Delete all cards")
        self.add_more_cards_button.clicked.connect(self.delete_flashcards)
        self.add_more_layout.addWidget(self.add_more_cards_button)

        # Question Area
        self.question_label = CopyableLabel("All caught up")
        font = self.question_label.font()  # Increase font size using QFont
        font.setPointSize(20)
        self.question_label.setFont(font)
        self.question_label.setAlignment(Qt.AlignCenter)
        self.question_label.setWordWrap(True)
        self.layout.addWidget(self.question_label)

        self.play_audio_button = QPushButton("Play Audio")
        self.play_audio_button.clicked.connect(self.play_audio)
        self.layout.addWidget(self.play_audio_button)

        # Answer Area
        self.answer_area = QWidget()
        self.answer_area.setMinimumHeight(50)
        self.layout.addWidget(self.answer_area)
        self.answer_layout = QVBoxLayout(self.answer_area)

        self.definition_label = QLabel("")
        self.definition_label.setAlignment(Qt.AlignCenter)
        self.definition_label.setWordWrap(True)
        self.answer_layout.addWidget(self.definition_label)

        # Buttons Area
        self.buttons_area = QWidget()
        self.buttons_layout = QHBoxLayout(self.buttons_area)
        self.layout.addWidget(self.buttons_area)

        self.show_answer_button = QPushButton("Show Answer")
        self.show_answer_button.clicked.connect(lambda: self.show_answer(True))
        self.buttons_layout.addWidget(self.show_answer_button)

        self.review_buttons_area = QWidget()
        self.buttons_layout.addWidget(self.review_buttons_area)
        self.review_buttons_layout = QHBoxLayout(self.review_buttons_area)

        self.again_button = QPushButton("Again")
        self.again_button.clicked.connect(lambda: self.review_flashcard(Rating.Again))
        self.review_buttons_layout.addWidget(self.again_button)

        self.hard_button = QPushButton("Hard")
        self.hard_button.clicked.connect(lambda: self.review_flashcard(Rating.Hard))
        self.review_buttons_layout.addWidget(self.hard_button)

        self.good_button = QPushButton("Good")
        self.good_button.clicked.connect(lambda: self.review_flashcard(Rating.Good))
        self.review_buttons_layout.addWidget(self.good_button)

        self.easy_button = QPushButton("Easy")
        self.easy_button.clicked.connect(lambda: self.review_flashcard(Rating.Easy))
        self.review_buttons_layout.addWidget(self.easy_button)

        # extra content Area
        self.extra_context_area_container = QWidget()
        self.extra_context_area_container.setMinimumHeight(400)
        self.layout.addWidget(self.extra_context_area_container)
        self.extra_context_area_container_layout = QVBoxLayout(
            self.extra_context_area_container
        )

        self.extra_context_area = QWidget()
        self.extra_context_area_container_layout.addWidget(self.extra_context_area)
        self.extra_context_area_layout = QVBoxLayout(self.extra_context_area)

        self.answer_buttons_area = QWidget()
        self.answer_buttons_layout = QHBoxLayout(self.answer_buttons_area)
        self.extra_context_area_layout.addWidget(self.answer_buttons_area)

        self.open_google_translate_button = QPushButton("Open Translate")
        self.open_google_translate_button.clicked.connect(self.open_google_translate)
        self.answer_buttons_layout.addWidget(self.open_google_translate_button)

        self.open_google_images_button = QPushButton("Open Images")
        self.open_google_images_button.clicked.connect(self.open_google_images)
        self.answer_buttons_layout.addWidget(self.open_google_images_button)

        # to find the file url, go to page on uakino, click on the dub, go to html source
        # find ashdi.vip, copy the video number, in new tab open https://ashdi.vip/vod/49666?nopl,
        # the ?nopl is important (geo locking), find the file: in playjs parameters
        # video_url = "https://test-streams.mux.dev/x36xhzz/x36xhzz.m3u8"
        # video_url = "https://ashdi.vip/video11/1/films/la_bete_2023_ukrfre_bdripavc_hurtom_132847/hls/BKeJlHaKkPtdmwzhAY8=/index.m3u8"
        video_url = "https://ashdi.vip/video11/1/films/1.1._harry_potter_and_the_sorcerers_stone_theatrical_cut_2001_bdrip_1080p_4xukr_eng_hurtom_49666/hls/BKeJlHaKkPtdmwzhAY8=/index.m3u8"
        self.videoPlayerWidget = VideoPlayerWidget(video_url)
        self.extra_context_area_layout.addWidget(self.videoPlayerWidget)

        self.play_video_clip_button = QPushButton("Replay")
        self.play_video_clip_button.clicked.connect(self.play_video_clip)
        self.extra_context_area_layout.addWidget(self.play_video_clip_button)

        self.spacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.layout.addItem(self.spacer)  # Add the spacer item

        self.load_next_card()

    def load_next_card(self):
        self.show_answer(False)
        self.current_card = self.fetch_earliest_due_card()
        if self.current_card:
            self.current_example = self.fetch_random_wordform(self.current_card.lemma)
            question = self.highlight_word(
                self.current_example.sentence.text, self.current_example.text
            )
            self.question_label.setText(question)
        else:
            self.question_label.setText("No flashcards available")

    def highlight_word(self, sentence, target_word):
        return re.sub(
            r"\b{}\b".format(re.escape(target_word)),
            r"<u>{}</u>".format(target_word),
            sentence,
        )

    def show_answer(self, show_answer):
        self.definition_label.setVisible(show_answer)
        self.show_answer_button.setVisible(not show_answer)
        self.review_buttons_area.setVisible(show_answer)
        self.extra_context_area.setVisible(show_answer)
        if show_answer:
            translation = self.translate_word(self.current_example.text)
            self.definition_label.setText(translation)
            self.play_video_clip()

    def review_flashcard(self, rating):
        card = self.current_card.to_card()
        card, review_log = self.fsrs.review_card(card, rating)
        self.current_card.from_card(card)
        self.current_card.save()
        self.load_next_card()

    def fetch_earliest_due_card(self):
        try:
            now = datetime.utcnow()
            query = (
                Flashcard.select()
                .where(Flashcard.due <= now)
                .order_by(Flashcard.due.asc())
                .limit(1)
            )
            flashcard = query.get()
            return flashcard
        except Flashcard.DoesNotExist:
            return None

    def fetch_random_wordform(self, lemma):
        word_forms = (
            WordForm.select()
            .join(Lemma)
            .switch(WordForm)
            .join(Sentence)
            .where(WordForm.lemma.text == lemma)
        )
        return random.choice(word_forms) if word_forms else None

    def add_flashcards(self, part_of_speech="ALL"):
        already_added = [fc.lemma for fc in Flashcard.select()]
        lemmas = self.get_top_lemmas(already_added, part_of_speech)

        new_flashcards = []
        for lemma in lemmas:
            new_flashcard = Flashcard.create__new_flashcard(lemma).to_dict()
            new_flashcards.append(new_flashcard)
        Flashcard.insert_many(new_flashcards).execute()

        self.load_next_card()

    def delete_flashcards(self):
        Flashcard.delete().execute()
        self.load_next_card()

    def get_top_lemmas(self, skip_lemmas=[], part_of_speech="ALL", number=10):
        # Start with the WordForm query and join with Lemma to get lemma text
        query = WordForm.select(Lemma.text, fn.COUNT(WordForm.id).alias("count")).join(
            Lemma
        )

        # Filter by part of speech if specified
        if part_of_speech != "ALL":
            query = query.where(WordForm.pos == part_of_speech)

        # Filter by skipped lemmas
        query = query.where(~(Lemma.text.in_(skip_lemmas)))

        # Group by lemma, count occurrences, and order by count
        query = (
            query.group_by(Lemma.text)
            .order_by(fn.COUNT(WordForm.id).desc())
            .limit(number)
        )

        # Retrieve only the lemma strings
        top_lemmas = [entry.lemma.text for entry in query]

        return top_lemmas

    def play_audio(self):
        if self.current_example:
            TTS.text_to_speech(self.current_example.sentence.text)

    def play_video_clip(self):
        self.videoPlayerWidget.set_start_time(self.current_example.sentence.timestamp)
        QTimer.singleShot(8000, self.videoPlayerWidget.pause_video)

    def translate_word(self, word, source_lang="uk", target_lang="en"):
        translation = Translator().translate(word, src=source_lang, dest=target_lang)
        return translation.text

    def open_google_translate(self, source_lang="uk", target_lang="en"):
        encoded_word = urllib.parse.quote(self.current_example.text)
        url = f"https://translate.google.com/?sl={source_lang}&tl={target_lang}&text={encoded_word}&op=translate"
        webbrowser.open(url)

    def open_google_images(self, source_lang="uk"):
        encoded_word = urllib.parse.quote(self.current_example.text)
        url = (
            f"https://www.google.com/search?tbm=isch&hl={source_lang}&q={encoded_word}"
        )
        webbrowser.open(url)


class CopyableLabel(QLabel):
    def __init__(self, text="", parent=None):
        super().__init__(text, parent)
        self.setTextInteractionFlags(Qt.TextSelectableByMouse)

    def mousePressEvent(self, event: QMouseEvent):
        if event.button() == Qt.LeftButton:
            clipboard = QApplication.clipboard()
            clipboard.setText(self.text())
        super().mousePressEvent(event)
