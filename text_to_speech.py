import os
import hashlib
import requests
import pygame
from dotenv import load_dotenv


class TTS:
    def load_api_key():
        load_dotenv()
        return os.getenv("ELEVEN_LABS_API_KEY")

    def load_voice_id():
        load_dotenv()
        return os.getenv("ELEVEN_LABS_VOICE_ID")

    def generate_tts(text, api_key, voice_id):
        url = f"https://api.elevenlabs.io/v1/text-to-speech/{voice_id}"
        headers = {
            "Accept": "audio/mpeg",
            "Content-Type": "application/json",
            "xi-api-key": api_key,
        }
        data = {
            "text": text,
            "model_id": "eleven_multilingual_v2",
            "voice_settings": {"stability": 0.5, "similarity_boost": 0.5},
        }
        try:
            response = requests.post(url, json=data, headers=headers, verify=False)
            response.raise_for_status()
            return response.content
        except requests.exceptions.HTTPError as err:
            print(f"HTTP error occurred: {err}")
        except Exception as err:
            print(f"An error occurred: {err}")
        return None

    def save_audio_file(audio_data, filename):
        directory = os.path.dirname(filename)
        if not os.path.exists(directory):
            os.makedirs(directory)
        with open(filename, "wb") as file:
            file.write(audio_data)

    def play_audio(filename):
        pygame.mixer.init()
        pygame.mixer.music.load(filename)
        pygame.mixer.music.play()
        while pygame.mixer.music.get_busy():
            pygame.time.Clock().tick(10)

    def hash_text(text):
        return hashlib.md5(text.encode()).hexdigest()

    def get_audio_filename(text):
        hashed_text = TTS.hash_text(text)
        return f"audio/{hashed_text}.mp3"

    def text_to_speech(text):
        audio_filename = TTS.get_audio_filename(text)

        if not os.path.exists(audio_filename):
            api_key = TTS.load_api_key()
            voice_id = TTS.load_voice_id()
            audio_data = TTS.generate_tts(text, api_key, voice_id)
            if audio_data:
                TTS.save_audio_file(audio_data, audio_filename)

        TTS.play_audio(audio_filename)
