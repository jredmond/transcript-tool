# To install the required packages, run the following commands:
# Install spacy for NLP:
# pip install spacy
#
# Install the Spacy models for English and Ukrainian:
# python -m spacy download en_core_web_sm
# python -m spacy download uk_core_news_sm
#
# Install googletrans for translation:
# pip install googletrans==4.0.0-rc1
#
# Install PySide6 for GUI:
# pip install PySide6
#
# Install dotenv to load the .env file for api keys:
# pip install python-dotenv
#
# pip install python-vlc

import re
from PySide6.QtWidgets import (
    QApplication,
    QMainWindow,
    QWidget,
    QVBoxLayout,
    QHBoxLayout,
    QLabel,
    QComboBox,
    QPushButton,
    QCheckBox,
    QTextEdit,
    QTreeWidget,
    QTreeWidgetItem,
    QTableWidget,
    QTableWidgetItem,
    QHeaderView,
    QFrame,
    QGridLayout,
    QMessageBox,
    QAbstractItemView,
    QTabWidget,
)
from PySide6.QtCore import QTimer
import sys
import os
from models import *
from flashcards import FlashcardWidget
from process_transcript import Processor


class MostCommonWordsAnalysisTool(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setWindowTitle("Most Common Words Analysis Tool")

        self.tab_widget = QTabWidget()
        self.setCentralWidget(self.tab_widget)

        self.flashcard_widget = FlashcardWidget()
        self.tab_widget.addTab(self.flashcard_widget, "Flashcards")

        central_widget = QWidget()
        self.tab_widget.addTab(central_widget, "Processor")
        main_layout = QGridLayout(central_widget)

        # Left frame for file selection, options, and table
        left_frame = QFrame()
        left_layout = QVBoxLayout(left_frame)
        main_layout.addWidget(left_frame, 0, 0)
        main_layout.setColumnStretch(0, 1)
        main_layout.setColumnStretch(1, 1)
        main_layout.setRowStretch(0, 1)

        # Top section of left frame for file and options
        top_left_frame = QFrame()
        top_left_layout = QVBoxLayout(top_left_frame)
        left_layout.addWidget(top_left_frame)

        # Set up file selection
        file_label = QLabel("Select Transcript File:")
        top_left_layout.addWidget(file_label)

        self.file_table = QTreeWidget()
        self.file_table.setHeaderLabels(["File"])
        top_left_layout.addWidget(self.file_table)

        # Bottom section of left frame for language, POS selection, analyze button, and table
        bottom_left_frame = QFrame()
        bottom_left_layout = QVBoxLayout(bottom_left_frame)
        left_layout.addWidget(bottom_left_frame)

        language_layout = QHBoxLayout()
        bottom_left_layout.addLayout(language_layout)

        language_label = QLabel("Language:")
        language_layout.addWidget(language_label)

        self.language_combo = QComboBox()
        self.language_combo.addItems(["Українська", "English"])
        self.language_combo.setCurrentIndex(0)  # Set Ukrainian as default
        language_layout.addWidget(self.language_combo)

        pos_layout = QHBoxLayout()
        bottom_left_layout.addLayout(pos_layout)

        pos_label = QLabel("Part of Speech:")
        pos_layout.addWidget(pos_label)

        self.pos_combo = QComboBox()
        pos_layout.addWidget(self.pos_combo)

        analyze_button = QPushButton("Analyze")
        pos_layout.addWidget(analyze_button)

        self.filter_known_checkbox = QCheckBox("Filter Known Lemmas")
        self.filter_known_checkbox.setChecked(True)
        bottom_left_layout.addWidget(self.filter_known_checkbox)

        # Table for displaying words
        self.table = QTableWidget()
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels(["Word", "Count"])
        self.table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.table.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.table.setSelectionMode(QAbstractItemView.SingleSelection)
        bottom_left_layout.addWidget(self.table)

        # Right frame for example sentences
        right_frame = QFrame()
        right_layout = QVBoxLayout(right_frame)
        main_layout.addWidget(right_frame, 0, 1)

        self.example_text = QTextEdit()
        self.example_text.setReadOnly(True)
        right_layout.addWidget(self.example_text)

        # Connect signals
        analyze_button.clicked.connect(self.on_analyze_button)
        self.pos_combo.currentIndexChanged.connect(self.on_change_part_of_speech)
        self.filter_known_checkbox.stateChanged.connect(self.refresh_table)
        self.table.itemSelectionChanged.connect(self.on_select_lemma)

        QTimer.singleShot(0, self.refresh_ui)

    def refresh_ui(self):
        self.refresh_file_table()
        self.refresh_pos_combo()
        self.refresh_table()
        self.refresh_example_sentences()

    def refresh_file_table(self):
        self.file_table.clear()
        for file in os.listdir("transcripts"):
            if file.endswith(".txt") or file.endswith(".srt"):
                QTreeWidgetItem(self.file_table, [file])
        if self.file_table.topLevelItemCount() > 0:
            self.file_table.setCurrentItem(self.file_table.topLevelItem(0))

    def refresh_pos_combo(self):
        pos_tags = self.load_part_of_speech_tags()
        self.pos_combo.clear()
        self.pos_combo.addItem("ALL")
        self.pos_combo.addItems(sorted(pos_tags))

    def load_part_of_speech_tags(self):
        return [word_form.pos for word_form in WordForm.select(WordForm.pos).distinct()]

    def refresh_table(self):
        pos_to_filter = self.pos_combo.currentText()
        filter_known = self.filter_known_checkbox.isChecked()
        filtered_words = self.load_word_table(pos_to_filter, filter_known)
        self.table.setRowCount(0)
        for lemma, count in filtered_words:
            row_position = self.table.rowCount()
            self.table.insertRow(row_position)
            self.table.setItem(row_position, 0, QTableWidgetItem(lemma))  # Lemma
            self.table.setItem(row_position, 1, QTableWidgetItem(str(count)))  # Count

    def load_word_table(self, part_of_speech="ALL", filter_skipped=False):
        # Start with the WordForm query and join with Lemma to get lemma text
        word_forms_query = WordForm.select(Lemma.text, WordForm.pos).join(Lemma)

        # Filter by part of speech if specified
        if part_of_speech != "ALL":
            word_forms_query = word_forms_query.where(WordForm.pos == part_of_speech)

        # Filter by skipped lemmas if specified
        if filter_skipped:
            skipped_lemmas = SkippedLemma.select(SkippedLemma.lemma)
            word_forms_query = word_forms_query.where(~(Lemma.text.in_(skipped_lemmas)))

        # Group by lemma and count occurrences
        word_forms_query = (
            word_forms_query.group_by(Lemma.text)
            .select(Lemma.text, fn.COUNT(WordForm.id).alias("count"))
            .order_by(SQL("count").desc())
        )

        # Return the list of (lemma.text, count) pairs
        return [
            (word_form.lemma.text, word_form.count) for word_form in word_forms_query
        ]

    def refresh_example_sentences(self):
        self.example_text.clear()
        selected_items = self.table.selectedItems()
        if selected_items:
            selected_lemma = selected_items[
                0
            ].text()  # Get the selected word from the table
            examples = self.load_examples_for_word(selected_lemma)

            if examples:
                self.example_text.setHtml(
                    "<br>".join(
                        [
                            f"{example.text}: {self.highlight_word(example.sentence.text, example.text)}"
                            for example in examples
                        ]
                    )
                )
            else:
                self.example_text.setPlainText("No sentence found.")

    def load_examples_for_word(self, lemma_text):
        lemma_entry = Lemma.get(Lemma.text == lemma_text)
        word_forms = (
            WordForm.select(WordForm, Sentence)
            .join(Sentence)
            .where(WordForm.lemma == lemma_entry)
        )
        return word_forms

    def highlight_word(self, sentence, target_word):
        return re.sub(
            r"\b{}\b".format(re.escape(target_word)),
            r"<u>{}</u>".format(target_word),
            sentence,
        )

    def on_analyze_button(self):
        # Get the selected file
        selected_items = self.file_table.selectedItems()
        if not selected_items:
            QMessageBox.critical(
                self.window, "Error", "Please select a transcript file."
            )
            return

        transcript_file = selected_items[0].text(0)
        selected_language = self.language_combo.currentText()

        Processor.process_and_store_transcript_data(transcript_file, selected_language)

        self.flashcard_widget.load_next_card()

        self.refresh_ui()

    def on_change_part_of_speech(self):
        self.refresh_table()

    def on_select_lemma(self):
        self.refresh_example_sentences()


def setup_gui():
    app = QApplication(sys.argv)
    window = MostCommonWordsAnalysisTool()
    window.show()
    sys.exit(app.exec())


setup_gui()
