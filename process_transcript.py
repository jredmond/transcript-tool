import re
import spacy
from datetime import datetime
from models import *


class Processor:

    def load_spacy_model(language):
        spacy_models = {"English": "en_core_web_sm", "Українська": "uk_core_news_sm"}
        selected_model = spacy_models[language]
        print(selected_model)
        return spacy.load(selected_model)

    def extract_sentences_from_file(transcript_file):
        def parse_timestamp(timestamp):
            return datetime.strptime(timestamp, "%H:%M:%S,%f").time()

        # Read the transcript file
        with open(f"transcripts/{transcript_file}", "r", encoding="utf-8") as file:
            transcript = file.read()

        sentences = []
        if transcript_file.endswith(".srt"):
            pattern = re.compile(
                r"(\d{2}:\d{2}:\d{2},\d{3}) --> (\d{2}:\d{2}:\d{2},\d{3})\n(.*?)\n\n",
                re.DOTALL,
            )
            matches = pattern.findall(transcript)

            for start_time, end_time, text in matches:
                text = re.sub(r"^- .*$", "", text, flags=re.MULTILINE)
                text = re.sub(r"\n", " ", text).strip()
                if text:
                    sentences.append((text, parse_timestamp(start_time)))
        else:
            # Handle non-SRT files
            # Define the regular expression pattern for sentence endings
            pattern = r"(?<=[.!?])\s+"

            # Split the transcript into sentences and trim whitespace from each sentence
            sentences = [
                (sentence.strip(), None) for sentence in re.split(pattern, transcript)
            ]

        return sentences

    def process_and_store_transcript_data(transcript_filename, language="Українська"):
        nlp = Processor.load_spacy_model(language)
        sentences = Processor.extract_sentences_from_file(transcript_filename)

        lemma_entries = {}
        word_form_entries = []
        sentence_entries = []

        for sentence_text, timestamp in sentences:
            doc = nlp(sentence_text)

            for token in doc:
                if token.is_punct:
                    continue

                word = token.text
                lemma = token.lemma_
                pos = token.pos_
                morph = str(token.morph)

                if lemma not in lemma_entries:
                    lemma_entries[lemma] = {"text": lemma}

                word_form_entries.append(
                    {
                        "text": word,
                        "pos": pos,
                        "morph": morph,
                        "lemma": lemma,
                        "sentence": sentence_text,
                    }
                )

            sentence_entries.append({"text": sentence_text, "timestamp": timestamp})

        with db.atomic():
            Lemma.delete().execute()
            Lemma.insert_many(lemma_entries.values()).execute()

            Sentence.delete().execute()
            Sentence.insert_many(sentence_entries).execute()

            lemma_instances = {lemma.text: lemma for lemma in Lemma.select()}
            sentence_instances = {
                sentence.text: sentence for sentence in Sentence.select()
            }

            word_form_entries_resolved = [
                {
                    "text": entry["text"],
                    "pos": entry["pos"],
                    "morph": entry["morph"],
                    "lemma": lemma_instances[entry["lemma"]],
                    "sentence": sentence_instances[entry["sentence"]],
                }
                for entry in word_form_entries
            ]

            WordForm.delete().execute()
            WordForm.insert_many(word_form_entries_resolved).execute()
