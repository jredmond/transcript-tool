import vlc
import sys
from PySide6.QtWidgets import QWidget, QVBoxLayout, QPushButton, QSizePolicy
from PySide6.QtCore import QTimer


class VideoPlayerWidget(QWidget):
    def __init__(self, video_url, start_time=0, parent=None):
        super().__init__(parent)

        self.instance = vlc.Instance(
            "--preferred-resolution=640", "--network-caching=5000", "--http-reconnect"
        )
        self.mediaPlayer = self.instance.media_player_new()

        self.playButton = QPushButton("Play")
        self.playButton.clicked.connect(self.play_video)

        layout = QVBoxLayout()
        self.setLayout(layout)
        self.videoWidget = QWidget(self)
        self.videoWidget.setAutoFillBackground(True)
        self.videoWidget.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.videoWidget.setMinimumSize(640, 265)
        layout.addWidget(self.videoWidget)
        layout.addWidget(self.playButton)

        if sys.platform.startswith("linux"):
            self.mediaPlayer.set_xwindow(self.videoWidget.winId())
        elif sys.platform == "win32":
            self.mediaPlayer.set_hwnd(self.videoWidget.winId())
        elif sys.platform == "darwin":
            self.mediaPlayer.set_nsobject(self.videoWidget.winId())

        self.start_time = start_time

        self.media = self.instance.media_new(video_url)
        self.mediaPlayer.set_media(self.media)

        # self.mediaPlayer.play()
        # QTimer.singleShot(200, self.mediaPlayer.pause)  # Delay to ensure media is ready

    def set_start_time(self, start_time=None):
        if start_time is not None:
            start_time_ms = (
                start_time.hour * 3600 * 1000
                + start_time.minute * 60 * 1000
                + start_time.second * 1000
                + start_time.microsecond // 1000
            )
            self.mediaPlayer.play()
            self.ensure_playing()
            self.mediaPlayer.set_time(start_time_ms)
            self.adjust_video_widget_size()

    def adjust_video_widget_size(self):
        width, height = self.mediaPlayer.video_get_size(0)
        if width > 0 and height > 0:
            aspect_ratio = width / height
            fixed_width = 640
            calculated_height = int(fixed_width / aspect_ratio)
            self.videoWidget.setMinimumSize(fixed_width, calculated_height)

    def ensure_playing(self):
        # Wait until the media player is not buffering
        if self.mediaPlayer.get_state() == vlc.State.Buffering:
            QTimer.singleShot(100, self.ensure_playing)
        else:
            self.mediaPlayer.play()

    def play_video(self):
        if self.mediaPlayer.is_playing():
            self.mediaPlayer.pause()
            self.playButton.setText("Play")
        else:
            self.mediaPlayer.play()
            self.playButton.setText("Pause")

    def pause_video(self):
        if self.mediaPlayer.is_playing():
            self.mediaPlayer.pause()
            self.playButton.setText("Play")
